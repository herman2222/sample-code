<?php

namespace App\Repositories;

use App\Post;
use App\Traits\Image;
use Illuminate\Support\Facades\DB;

class PostRepository implements PostRepositoryInterface
{
    use Image;
    /**
     * Get's a post by it's ID
     *
     * @param int
     * @return collection
     */
    public function get($post_id)
    {
        return Post::find($post_id);
    }

    /**
     * Get's all posts.
     *
     * @return mixed
     */
    public function all()
    {
        return Post::all();
    }

    /**
     * Deletes a post.
     *
     * @param int
     */
    public function delete($post_id)
    {
        $post = Post::find($post_id);
        $post->delete();
        $post->tag()->delete($post_id);
        return;
    }

    /**
     * Updates a post.
     *
     * @param int
     * @param array
     */
    public function update($post_id, array $post_data)
    {
        $post = Post::find($post_id);
        if (isset($post_data['image'])){
            $this->deleteFromPath($post->image);
            $post_data['image'] = $this->saveImage($post_data['image']);
        }
        $post->update(array_filter($post_data));
        $post->tag()->sync($post_data['tags']);

        return;
    }

    /**
     * @param integer $paginate
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getWithRelations(int $paginate = null)
    {
        if (isset($paginate)){
            return Post::with(['tag','categorys','authors'])->paginate($paginate);
        }
        return Post::with(['tag','categorys','authors'])->all();
    }


    /**
     * @param $param
     * @param $value
     * @param null $paginate
     * @param string $order
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getWithRelationsWithParam($param, $value, $paginate = null, $order = 'id')
    {
        if (isset($paginate)){
            return Post::with(['tag','categorys','authors'])->where($param,$value)->orderBy($order,'asc')->paginate($paginate);
        }
        return Post::with(['tag','categorys','authors'])->where($param,$value)->get();
    }


    /**
     * @param $request
     * @param $model
     * @return \Exception|void
     */
    public function storeWithRelations($request, $model){
        try{
            DB::beginTransaction();
            $post = $model->create($request->all());
            $image = $this->saveImage($request->file('image'));
            $post->update(['image' => $image]);
            $post->tag()->sync((array)$request->input('tags'));
        }catch (\Exception $exception){
            DB::rollBack();
            return new \Exception($exception->getMessage());
        }
        DB::commit();
        return;
    }


    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    public function getWithRelationsById($id){
        return Post::with(['tag','categorys','authors'])->find($id);
    }

    public function updateStatus($id,$status){
        $post = Post::find($id);
        $post->update(['is_published'=>$status]);
        return;
    }
}