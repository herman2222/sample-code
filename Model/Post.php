<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    const Permission_Update = 'update-my-post';
    const Permission_Admin = 'Admin';
    const Published = 'Yes';
    const UnPublished = 'No';

    public static  function boot()
    {
        parent::boot();
        static::creating(function ($model){
            $model->author = auth()->user()->id;
        });
    }
    protected $fillable = ['title','content','image','category','author','is_published'];
    public function tag()
    {
        return $this->belongsToMany(Tag::class, 'post_tag');
    }
    public function categorys(){
        return $this->belongsTo(Category::class, 'category');
    }
    public function authors(){
        return $this->belongsTo(User::class, 'author');
    }

}
