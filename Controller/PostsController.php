<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\PostRequest;
use App\Post;
use App\Repositories\PostRepository;
use App\Tag;
use App\User;
use Illuminate\Support\Facades\Gate;


class PostsController extends Controller
{
    protected $post;

    /**
     * PostController constructor.
     *
     * @param PostRepository $post
     */
    public function __construct(PostRepository $post)
    {
        $this->post = $post;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (\auth()->user()->hasAnyRole(User::Role_Admin)){
            return view('posts.index', ['posts' => $this->post->getWithRelations(15)]);
        }
        return view('posts.index', ['posts' => $this->post->getWithRelationsWithParam('author',\auth()->user()->id,15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Category $category
     * @param Tag $tag
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category, Tag $tag)
    {
        return view('posts.create', compact('category','tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest $request
     * @param Post $model
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request, Post $model)
    {
        $this->post->storeWithRelations($request,$model);
        return redirect()->route('posts.index')->withStatus(__('Post successfully created.'));;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $posts
     * @return \Illuminate\Http\Response
     */
    public function show(Post $posts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post $posts
     * @param $id
     * @param Tag $tag
     * @param Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $posts,$id,Tag $tag,Category $category)
    {
        if (Gate::any([Post::Permission_Update,Post::Permission_Admin], $posts->find($id))) {
            $post = $this->post->getWithRelationsById($id);
            return view('posts.edit', compact('tag','post','category'));
        }
        return view('errors.403');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest $request
     * @param  \App\Post $posts
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $posts,$id)
    {
        $this->post->update($id,$request->all());
        return redirect()->route('posts.index')->withStatus(__('Post successfully updated.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->post->delete($id);
        return redirect()->route('posts.index')->withStatus(__('Post successfully deleted.'));
    }

    /**
     * @param $id
     * @param $current
     */
    public function updateStatus($id, $current){
        if ($current == Post::Published){$status = Post::UnPublished;} else{$status = Post::Published;}
        $this->post->updateStatus($id,$status);
        return redirect()->route('posts.index')->withStatus(__('Post successfully updated.'));
    }
}
